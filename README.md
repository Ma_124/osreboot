osreboot(1) -- Reboot to different OS with GRUB.
================================================

SYNOPSIS
--------

`osreboot` [<var>flags</var>] <var>os</var>

OPTIONS
-------

* `-n`, `--now`:
  Reboot immediately
* `-H`, `--halt`:
  Halt instead of rebooting, only applies if `--now` is set
* `-g`, `--gui`:
  Use a graphical user interface
* `-f`, `--force`:
  Reboot after first match and do not search for collisions and do not wait for confirmation
* <var>os</var>:
  A part of the name of a GRUB menuentry

SECURITY CONSIDERATIONS
-----------------------

This program does not rely on `$PATH` but instead on absolute paths.
It accesses the following files which must be under the sole control of root:

* `/boot/grub/grub.cfg`
* `/usr/sbin/grub-reboot`
* `/sbin/halt`
* `/sbin/reboot`
* `/usr/bin/qdbus` (for a graphical shutdown: `/usr/bin/qdbus org.kde.ksmserver /KSMServer logout`)

BUGS
----

There are no known bugs.

Bugs should be reported to https://gitlab.com/Ma_124/osreboot/-/issues if they don't impact the security of this program
even if it is executed with the sticky bit set.

Otherwise, email ma_124+oss@pm.me and do not publicise the issue before it can be resolved.

AUTHOR
------

Ma_124, <https://ma124.js.org> <ma_124+oss@pm.me>

COPYRIGHT
---------

Copyright 2020 Ma_124, <https://ma124.js.org> <ma_124+oss@pm.me>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SEE ALSO
--------

grub-reboot(8), reboot(8), halt(8)


[grub-reboot(8)]: https://manpages.debian.org/stretch/grub2-common/grub-reboot.8.en.html
[reboot(8)]: http://man7.org/linux/man-pages/man8/reboot.8.html
[halt(8)]: http://man7.org/linux/man-pages/man8/halt.8.html
[osreboot.1.md]: osreboot.1.md.html
