// Copyright 2020 Ma_124, ma124.js.org <ma_124+oss@pm.me>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package main

//go:generate sh -c 'ronn --organization Ma_124 --date 2020-03-18 --style print -r -5 --markdown osreboot.1.md && rm -f README.md && mv osreboot.1.markdown README.md'

import (
	"bufio"
	"bytes"
	"fmt"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"unicode"
)

var (
	app = kingpin.New("osreboot", "Reboot to different OS with GRUB.")
	now = app.Flag("now", "Reboot immediately").Short('n').Bool()
	halt = app.Flag("halt", "Halt instead of rebooting").Short('H').Bool()
	gui = app.Flag("gui", "Use a graphical user interface").Short('g').Bool()
	force = app.Flag("force", "Reboot after first match and do not search for collisions and do not wait for confirmation").Short('f').Bool()
	osn = app.Arg("os", "A part of the name of a GRUB menuentry").Required().String()

	code int
)

func main() {
	defer os.Exit(code)

	kingpin.MustParse(app.Parse(os.Args[1:]))

	f, err := os.Open("/boot/grub/grub.cfg")
	herr(err, true)
	defer func() {
		herr(f.Close(), false)
	}()

	osnbs := bytes.ToLower([]byte(*osn))

	sc := bufio.NewScanner(f)
	i := 0
	si := -1
	sn := ""
	for sc.Scan() {
		l := bytes.TrimSpace(sc.Bytes())
		if bytes.HasPrefix([]byte("menuentry"), l) {
			i++
			l = l[len("menuentry"):]
			l = bytes.TrimFunc(l, func(r rune) bool {
				return unicode.IsSpace(r) || r == '\''
			})
			name := l[:bytes.IndexByte(l, '\'')]
			// TODO --class
			if bytes.Contains(name, osnbs) {
				if si != -1 {
					herr(fmt.Errorf("collision between menuentry %q (%v) and %q (%v", sn, si, name, i), true)
					return
				}
				si = i
				sn = string(name)
				if *force {
					break
				}
			}
		}
	}
	herr(sc.Err(), true)
	if si == -1 {
		herr(fmt.Errorf("no OS %q found", osn), true)
		return
	}

	herr(execCmd(exec.Command("/usr/sbin/grub-reboot", strconv.Itoa(i))), true)

	if *now {
		var cmd []string
		if !*gui {
			if *halt {
				cmd = []string{"/sbin/halt"}
			} else {
				cmd = []string{"/sbin/reboot"}
			}
		} else {
			cmd = make([]string, 0, 7)
			cmd = append(cmd, "/usr/bin/qdbus", "org.kde.ksmserver", "/KSMServer", "logout")
			if *force {
				// don't wait for confirmation
				cmd = append(cmd, "0")
			} else {
				cmd = append(cmd, "1")
			}
			if *halt {
				cmd = append(cmd, "2")
			} else {
				cmd = append(cmd, "1")
			}
			cmd = append(cmd, "0")
		}
		herr(execCmd(exec.Command(cmd[0], cmd[1:]...)), true)
	}
}
func execCmd(cmd *exec.Cmd) error {
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	return cmd.Run()
}

func herr(err error, fatal bool) {
	if err != nil {
		app.Errorf("Error: " + err.Error())
		if *gui {
			// TODO msgbox
		}
		if fatal {
			code = 2
			runtime.Goexit()
		}
	}
}

